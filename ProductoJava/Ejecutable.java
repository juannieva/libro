public class Ejecutable{
    public static void determinarMayor(Producto producto1, Producto producto2){
        if(producto1.getPreciodeVenta()>producto2.getPreciodeVenta()){
            System.out.println("\nEl producto de mayor precio es: "+producto1.getNombProducto()+" con un precio de:"+producto1.getPreciodeVenta());
        }else{
            System.out.println("\nEl producto de mayor precio es: "+producto2.getNombProducto()+" con un precio de:"+producto2.getPreciodeVenta());
        }
        }
    public static void main(String[] args) {
        
        Producto producto1 = new Producto();
        producto1.setCodigoProducto(4231);
        producto1.setNombProducto("Papas Fritas");
        producto1.setPorcentajedeGanancia(20);
        producto1.setPrecioCosto(600);        
        producto1.mostrarInformacion();
        Producto producto2 = new Producto();
        producto2.setCodigoProducto(7243);
        producto2.setNombProducto("Ketchup");
        producto2.setPorcentajedeGanancia(20);
        producto2.setPrecioCosto(500);        
        producto2.mostrarInformacion();
        determinarMayor(producto1,producto2);
    }
        
}

