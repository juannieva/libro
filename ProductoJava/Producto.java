import java.nio.FloatBuffer;

public class Producto{
    private int codigoProducto;
    private String nombProducto;
    private float precioCosto;
    private float porcentajedeGanancia;
    private float preciodeVenta;

    public void mostrarInformacion(){
        System.out.println("----------------------------------\nCodigo del producto: "+ getCodigoProducto() +
         "\nNombre del Producto: "+ getNombProducto()+
         "\nPrecio de Costo: " + getPrecioCosto()+
         "\nPorcentaje de Ganacia: "+getPorcentajedeGanancia()+
         "\nIva: 21 \nPrecio de Venta: "+ getPreciodeVenta()+"\n----------------------------------");
    }

    public float calculoPrecio(float precioCosto){
        float iva;
        iva=21;
        iva=(iva/100)+1;
        float porcentajeGananciaSumar;
        porcentajeGananciaSumar=(precioCosto*porcentajedeGanancia)/100;
        this.preciodeVenta=(precioCosto+porcentajeGananciaSumar)*iva;  
        return preciodeVenta;      
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombProducto() {
        return nombProducto;
    }

    public void setNombProducto(String nombProducto) {
        this.nombProducto = nombProducto;
    }

    public float getPrecioCosto() {
        return precioCosto;
    }

    public void setPrecioCosto(float precioCosto) {
        this.precioCosto = precioCosto;
        setPreciodeVenta(precioCosto);
    }

    public float getPorcentajedeGanancia() {
        return porcentajedeGanancia;
    }

    public void setPorcentajedeGanancia(float porcentajedeGanancia) {
        this.porcentajedeGanancia = porcentajedeGanancia;
    }

    public float getPreciodeVenta() {
        return preciodeVenta;
    }

    public void setPreciodeVenta(float precioCosto) {
        this.preciodeVenta = calculoPrecio(precioCosto) ;
    }
    
}