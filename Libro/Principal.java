public class Principal {
    public static void main(String[] args) {
        Libro libro1 = new Libro();
        Libro libro2 = new Libro();
        libro1.setIsbn(220087);
        libro1.setAutor("Juan Luis Garcia");
        libro1.setNumPaginas(150);
        libro1.setTitulo("Superman sin capa");
        libro2.setIsbn(809921);
        libro2.setAutor("Jose Martinez");
        libro2.setNumPaginas(260);
        libro2.setTitulo("1000 Recetas Saludables");
        System.out.println("El libro "+ libro1.getTitulo() +" con ISBN "+ libro1.getIsbn() + " , creado por el autor: "+ libro1.getAutor() + ", tiene " + libro1.getNumPaginas() + " paginas.");  
        System.out.println("El libro "+ libro2.getTitulo() +" con ISBN "+ libro2.getIsbn() + " , creado por el autor: "+ libro2.getAutor() + ", tiene " + libro2.getNumPaginas() + " paginas.");     
    }
    
}